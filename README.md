# Lotto number generator for Norwegian lottery games. #
School assignment for introduction course in C-programming, fall 2010.

## Techologies / Topics ##
- pointers
- sorting of tables 
- random numbers

Extensive use of pointers in this applications. All comments in Norwegian.

## The program ##
![skjermdump1.JPG](https://bitbucket.org/repo/poeedB/images/2180137095-skjermdump1.JPG)

![skjermdump2.JPG](https://bitbucket.org/repo/poeedB/images/2092686721-skjermdump2.JPG)