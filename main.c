#include <stdio.h>
#include <stdlib.h>
#include <time.h>
/*     DATFE40 - Oblig 2 - Terje Rene E. Nilsen    */
#define VIKINGTALL 48
#define VIKINGANT 6
#define LOTTOTALL 34
#define LOTTOANT 7
int Valg();
int LesInn();
void Trekk(int *xPtr, int *antPtr, int *tallPtr);
int tallErUnik(int *xPtr, int *antPtr, int *rPtr);
void Sorter(int *xPtr, int *antPtr);
void SkrivUt(int *xPtr, int *antPtr);

int main(int argc, char *argv[]) {
    int ant = 0; /* antall tall */
    int maxverdi = 0; /* st�rste verdi per tall */
    int i; /* teller */
    int brukerValg; /* valg; lotto, viking eller avslutt */
    int antRekker; /* hvor mange rekker? */
    int x[8] = {0}; /* litt i overkant stor nok til begge typene */
    int *antPtr = &ant;
    int *tallPtr = &maxverdi;
    int *xPtr = x; /* Lager peker xPtr, og retter denne mot x-tabellen  
                                trenger ikke & forran x, da x allerede er en peker
                                til tabellen.
                                */
    srand(time(0));
    do {
    start:
    brukerValg=Valg();
    system ("CLS"); /* utseende: tom skjerm */
    printf ("Trekningstype: ");
    if (brukerValg==1) { //lotto
              printf ("LOTTO\n");
              ant=LOTTOANT-1;
              maxverdi=LOTTOTALL;
              }      
    else if (brukerValg==2) { //viking
              printf ("VIKINGLOTTO\n");
              ant=VIKINGANT-1;
              maxverdi=VIKINGTALL;
              }
    else { return 0;  /* avslutt */ }
    antRekker=LesInn();
    if (antRekker<1) { goto start; }
    printf ("%d tall per rekke.\n\nTREKNING:\n\n", ant+1);
     
    for (i=1; i<=antRekker; i++) {
        Trekk(xPtr, antPtr, tallPtr); /* kall trekkfunksjon, send med nodvendig parametre */    
        Sorter(xPtr, antPtr); /* kall sorterfunksjon, send med nodvendig parametre */ 
        SkrivUt(xPtr, antPtr);
    }  
    
  printf ("\nTrykk en tast for \x86 g\x86 tilbake til menyen...");       
  system ("PAUSE>0"); 
  system ("CLS"); /* utseende: tom skjerm */	
 } /* du rykker tilbake til start */
 while (1);
}

int LesInn() {
    /*
    Lar bruker bestemme antall rekker
    returnerer brukers inputt
    */
    int valg=0;
    printf ("Antall rekker: ");
    scanf ("%d", &valg);
    return valg;    
}

int Valg() {
    /*
    Lar bruker velge lottotype eller avslutte
    Gyldig valg returnerer tallverdi:
           lotto - 1
           vikinglotto - 2
           avslutte - 3
    Ugyldig valg gir tilsynelatende inget utfall pa skjermbilde
    */
    char valg=0;
    valgstart:
    printf ("Trekningstype:\n");
    printf ("L - Lotto\n");
    printf ("V - Vikinglotto\n-\n");
    printf ("A - Avslutt\n");
    scanf ("%c", &valg);
    if (valg == 'l' || valg == 'L') { return 1; }
    else if (valg == 'v' || valg == 'V') { return 2; }
    else if (valg == 'a' || valg == 'A') { return 3; }
    else { system ("CLS"); goto valgstart; }
}

void Trekk(int *xPtr, int *antPtr, int *tallPtr) {
    /*
    Trekker tilfeldig lottotall
    Gitte parametere bestemmer:
          antPtr - peker til variabel; antall tall i en rekke (main)
          tallPtr - peker til variabel; maxverdi per tall (main)
          xPtr - peker til tabell som fylles med lottorekke (main)
    */
     int i, *iPtr=&i, r, *rPtr = &r;
     for (i=0; i <= *antPtr; i++) {
         r=1+rand() % *tallPtr; /* r= tilfeldig tall mellom 1 og tallPtr */
         if (tallErUnik(xPtr, iPtr, rPtr)) { /* sjekker at tallet er unikt */
                xPtr[i]=r; /* fyller x-tabellen med lottotall */
         }
         else { i--; } /* ikke unikt, samme runde */
     } 
    
}

int tallErUnik(int *xPtr, int *antPtr, int *rPtr) {
    /*
    Sjekker at tallet er relativt unikt i forhold til tabellen.
    Returnerer 1 dersom tallet er unikt, 0 ellers
    Parametere:
               antPtr - peker til variabel; lovlige tall per rekke (main)
               xPtr - peker til tabell som inneholder rekke (main)
               rPtr - peker til variabel; tallet som skal sjekkes (Trekk)
    */
    int i;
    for (i=0; i <= *antPtr; i++) { /* for sjekker alle relaterte tall i tabellen */
        if (xPtr[i]==*rPtr) { return 0; break; } /* ikke unikt; returner 0; hopp ut av for
         bruker *rPtr isteden for rPtr for � sammenligne verdien til variabelen som pekes til
         og ikke adressen til variabelen */
    }
    return 1; /* hurra, et nytt unikt tall! */
} 

void Sorter(int *xPtr, int *antPtr) {
   /* 
    Sorterer gitt tabell med bubble sort algoritmen
    Parametere:
               xPtr - peker til tabell som skal sorteres (main)
               antPtr - peker til variabel; forteller hvor lang rekken er (main)
    */
 int i,u,mid;
 for (u=0; u<*antPtr; u++) {
     for (i=0; i<*antPtr; i++) {
         if (xPtr[i] > xPtr[i+1]) {
             mid = xPtr[i+1];
             xPtr[i+1] = xPtr[i];
             xPtr[i] = mid;
         }
     }
 }
}

void SkrivUt(int *xPtr, int *antPtr) {
   /* 
    Skriver ut rekken
    Parametere:
               xPtr - peker til tabell som skal sorteres (main)
               antPtr - peker til variabel; forteller hvor lang rekken er (main)
    */
     int u;
        printf ("\t"); /* utseende */
        for (u=0;u<=*antPtr;u++) { /* print en rekke med 'antPtr' tall fra x */
            if (xPtr[u]>9) { /* utseende - 2 siffer */
                  printf ("%d  ",xPtr[u]);
            }
            else { /* utseende */
                  printf (" %d  ",xPtr[u]); 
            }
        }
        printf ("\n");
}
